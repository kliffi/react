import React from 'react';
import '../../css/App.css';
import Index from './Index';
import Catalog from './Catalog';
import Product from './Product';
import { BrowserRouter as Router,  Route, Link  } from "react-router-dom";

class Main extends React.Component{
  render(){
    return (
      <Router>
        <div>
          <ul className="nav_elements">
              <Link to="/" className="nav_el"><li>Home</li></Link>
              <Link to="/about" className="nav_el"><li>About</li></Link>
              <Link to="/catalog" className="nav_el"><li>Catalog</li></Link>
              <Link to="/topics" className="nav_el"><li>Topics</li></Link>
          </ul>
            <hr />
          
            <Route exact path="/" component={Index} />
            <Route exact path="/catalog" component={Catalog} />
            <Route exact path="/product" component={Product} />
        
        </div>
      </Router>
    );
}
    
  }


 export default Main;
