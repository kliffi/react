import React from 'react';
import '../css/Slider.css';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';


function Slider() {
    const slider = (
        <AwesomeSlider bullets={false} transitionDelay={1}>
          <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
          <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
          <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
        </AwesomeSlider>
      );
  return (
      
    <div style={{height:`350px`}}>
    {slider}
    </div>
   
  );
      
}

export default Slider;
