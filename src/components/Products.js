import React from 'react';
import '../css/Products.css';
import 'react-awesome-slider/dist/styles.css';
import { BrowserRouter as Router,  Route, Link  } from "react-router-dom";


class Products extends React.Component{
    render(){
      let menus=[
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"1rfsda","price":15624},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"sdfasd","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"xzcvzxc","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"gfhtyu","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"mhgjgh","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"uyutttt","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"fghfvcc","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"bnmyumt","price":100},
      ]
      return (
        <div className="row">{menus.map((value,index)=>{
            return <div className="col-3" key={index}>{this.props.text}
            <div className="card  cardblock shadow-sm bg-white ">
            <div className="product_card_img" style={{background:`url(${value.img})`}}></div>
              <div>
                <div className="card-body">
                  <h5 className="card-title">{value.text}</h5>
                  <Link to={"/product?id="+index} className="link"><span className="btn btn-primary">{value.price} ₽</span></Link>
                </div>
              </div>
              </div>
            </div>
            })}
          </div>
      );
    }
}

export default Products;
