import React from 'react';
import '../../css/Catalog.css';
import 'react-awesome-slider/dist/styles.css';
import { BrowserRouter as Router,  Route, Link  } from "react-router-dom";
import AwesomeSlider from 'react-awesome-slider';
import Stocks from '../Stocks';

class Catalog extends React.Component{
    render(){
      let menus=[
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"1rfsda","price":15624},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"sdfasd","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"xzcvzxc","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"gfhtyu","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"mhgjgh","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"uyutttt","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"fghfvcc","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"bnmyumt","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"ergbbtyhn","price":100},
        {"img":"http://qnimate.com/wp-content/uploads/2014/03/images2.jpg","text":"d dgnfgh","price":100},
      ]
      return (
        <div className="container">
        <div className="row">
          <div className="col-10">
          <AwesomeSlider bullets={false} transitionDelay={1}>
            <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
            <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
            <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
          </AwesomeSlider>
          </div>        
          <div className="col-2"> <Stocks /></div>        
        </div>
        {menus.map((value,index)=>{
          return <Link to={"/product?id="+index} className="link" key={index} className="cat_el_a"><div className="cat_el shadow-sm p-3 mb-2 bg-white rounded"  role="alert">
          <img src={value.img} width="150px" alt={value.img}></img>
          <span className="p-3 ml-2 bg-white"> {value.text} </span> <span className="float-right align-text-bottom">{value.price} ₽</span>
          </div></Link>
            
          })}</div>
      );
    }
}

export default Catalog;
