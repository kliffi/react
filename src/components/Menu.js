import React from 'react';
import '../css/App.css';
import Catalog from './Catalog';

import { BrowserRouter as Router,  Route,Switch } from "react-router-dom";

class Menus extends React.Component{
  render(){
    let menus=[
      "home",
      "catalog",
      "hits",
      "fits",
      "fits lol",
    ]
    return <ul className="nav_elements">{menus.map((value,index)=>{
      var url="/"+value.trim().replace(" ","_");
  return <a className="nav_el" href={url} key={index}><li>{value}</li></a>
    })}</ul>
  }
}


// export default Menu;
export default function Menu() {
  return (
    <Router>
        <Menus />
        <Switch>
          <Route path="/home">
            <Catalog />
          </Route>
        </Switch>
    </Router>
  );
}
