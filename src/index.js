import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './components/main/Main';
/* import Menu from './components/Menu';
import Slider from './components/Slider';
import Products from './components/Products';
import Stocks from './components/Stocks'; */
import * as serviceWorker from './serviceWorker';


ReactDOM.render(<Main />, document.getElementById('root'));
/* ReactDOM.render(<Menu />, document.getElementById('nav_elements'));
ReactDOM.render(<Slider />, document.getElementById('slider'));
ReactDOM.render(<Products />, document.getElementById('products'));
ReactDOM.render(<Stocks />, document.getElementById('stocks')); */


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
