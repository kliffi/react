import React from 'react';
import '../../css/App.css';
import Products from '../Products';
import Stocks from '../Stocks';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';

class Index extends React.Component{
    render(){
        return (
              <div>
                <div className="container">
                  <div className="row">
                    <div className="col-8 slider">
                      <AwesomeSlider bullets={false} transitionDelay={1}>
                        <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
                        <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
                        <div data-src="http://qnimate.com/wp-content/uploads/2014/03/images2.jpg" />
                      </AwesomeSlider>
                    </div>
                    <div className="col-4">
                        <Stocks />
                    </div>
                  </div>
                </div>
                <div className="container">
                <Products />
                </div>
              </div>
          );
    }
}
 export default Index;
